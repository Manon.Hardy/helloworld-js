FROM node:slim

COPY . /app
WORKDIR /app

RUN npm install
CMD echo "kakou kakou !" && npm start
EXPOSE 3000
